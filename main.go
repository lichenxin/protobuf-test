package main

import (
	"fmt"
	"./protocol"
	"github.com/golang/protobuf/proto"
)

func main() {
	fmt.Println("levi")
	fmt.Println(proto.String("hello"))

	tsData := &protocol.WPacket_TS_TEST_DATA{
		Id: 1,
		Name: "levi",
		Age: 20,
	}

	// 编码
	jmData, err := proto.Marshal(tsData)
	if err != nil {
		fmt.Println("error:", err)
	}

	// 解码
	jsData := &protocol.WPacket_TS_TEST_DATA{}
	err = proto.Unmarshal(jmData, jsData)
	if err != nil {
		fmt.Println("error:", err)
	}

	fmt.Println(jsData, jsData.GetId(), jsData.Name, jsData.Age)
}